# Grupo 62
# 83417 - Afonso De Sousa Samora
# 83567 - Tiago Miguel Calhanas Goncalves
import copy
from search import *
# TAI
# Color (color): no color = 0; with color > 0
def get_no_color():
    return 0
def no_color(c):
    return c == 0
def color(c):
    return c > 0

# Position(pos): tuple(<row>, <column>)
def make_pos(l, c):
    return l, c
def pos_l(pos):
    return pos[0]
def pos_c(pos):
    return pos[1]

# Procura de grupos no tabuleiro
def notVisitedList(board):
    notVisitedTiles = []
    length = len(board)
    height = len(board[0])
    for x in range(0, length):
        for y in range(0, height):
            if color(board[x][y]):
                notVisitedTiles.append(make_pos(x, y))
    return notVisitedTiles

def board_find_groups(board):
    board_len = len(board)
    board_hei = len(board[0])
    groupedTiles = []
    notVisitedTiles = notVisitedList(board)
    while notVisitedTiles != []:
        l = pos_l(notVisitedTiles[0])
        c = pos_c(notVisitedTiles[0])
        color = board[l][c]
        tiles = []
        get_adjacent_tiles(color, l, c, board, tiles, notVisitedTiles)
        groupedTiles.append(tiles)
    return groupedTiles

def get_adjacent_tiles(color, x, y, board, group, notVisitedTiles):
    pos = make_pos(x, y)
    if pos not in notVisitedTiles:
        return
    if board[x][y] == color:
        notVisitedTiles.remove(pos)
        group.append(pos)
        get_adjacent_tiles(color, x + 1, y, board, group, notVisitedTiles)
        get_adjacent_tiles(color, x, y + 1, board, group, notVisitedTiles)
        get_adjacent_tiles(color, x - 1, y, board, group, notVisitedTiles)
        get_adjacent_tiles(color, x, y - 1, board, group, notVisitedTiles)
    else:
        return

def board_remove_group(board, group):
    new_board = copy.deepcopy(board)
    for pos in group:
        new_board[pos_l(pos)][pos_c(pos)] = get_no_color()
    compact(new_board)
    return new_board

def compact(board):
    compact_vertical(board)
    compact_horizontal(board)

def compact_vertical(board):
    linhas = len(board) - 1
    colunas = len(board[0])
    for y in range(0, colunas):
        counter = 0
        for x in range(linhas, -1, -1):
            if no_color(board[x][y]):
                counter = counter + 1
            else:
                board[x+counter][y],board[x][y] = board[x][y], board[x+counter][y]

def compact_horizontal(board):
    linhas = len(board) - 1
    colunas = len(board[0])
    counter = 0
    for y in range(0, colunas):
        if no_color(board[linhas][y]):
            counter += 1
        elif counter != 0:
            for x in range(linhas, -1, -1):
                board[x][y-counter], board[x][y] = board[x][y], board[x][y-counter]
    if counter == 0:
        return board

def board_find_groups_actions(board):
    groupedTiles = board_find_groups(board)
    aux = copy.deepcopy(groupedTiles)
    for sublist in aux:
        if len(sublist) < 2:
            groupedTiles.remove(sublist)
    return groupedTiles

def board_remove_group_result(board, group):
    resultBoard = board_remove_group(board, group)
    return sg_state(resultBoard)

def board_empty(board):
    linhas = len(board)
    colunas = len(board[0])
    for x in range(0, linhas):
        for y in range(0, colunas):
            if color(board[x][y]):
                return False
    return True

def group_number(board):
    return len(board_find_groups(board))

class sg_state:
    def __init__(self, board):
        self.board = board

    def __lt__(self, state2):
        return group_number(self.board) < group_number(state2.board)

class same_game(Problem):
    def __init__(self, initial):
        self.initial = sg_state(initial)

    def actions(self, state):
        return board_find_groups_actions(state.board)

    def result(self, state, action):
        return board_remove_group_result(state.board, action)

    def goal_test(self, state):
        return board_empty(state.board)

    def path_cost(self, c, state1, action, state2):
        return c + 1

    def h(self, node):
        return group_number(node.state.board)
